<?php
	// Crea la Conexion
	include_once ('ejercicio_1_config.php');

	if (isset($_GET['id']) && !empty($_GET['id'])) {
		// Get hidden input value
		$id = $_GET["id"];

		$resultado = pg_query($conn, "SELECT * FROM producto WHERE producto_id = $id");
		$resultado = pg_fetch_row($resultado);
		
		// Obtiene los resultados a editar
		$nombre = $resultado[3];
		$descripcion = $resultado[4];
		$tipo = $resultado[1];
		$marca = $resultado[2];

		$marcas = pg_query($conn, "SELECT * FROM marca");
		$tipos = pg_query($conn, "SELECT * FROM tipo");

	$str=<<<HTML
	<form action="#" method="post">
			<div><b> INSERTAR PRODUCTO </b></div>
			<div> 
				<label for="nombre">Nombre del Producto:</label>
				<input type="text" name="nombre" value="$nombre">
			</div>
			<div> 
				<label for="descripcion">Descripción:</label>
				<input type="text" name="descripcion" value="$descripcion">
			</div>
			<div> 
				<label for="marca">Marca:</label>
				<select id="marca" name="marca">
	HTML;
				while ($row = pg_fetch_row($marcas)) {
					if ($marca == $row['0']) {
						$str .= "<option value='" . $row['0'] . "' selected>" . $row['1'] . "</option>";
					} else {
						$str .= "<option value='" . $row['0'] . "'>" . $row['1'] . "</option>";
					}
				}
	$str.=<<<HTML
				</select>
			</div>
			<div> 
				<label for="tipo">Tipo:</label>
				<select id="tipo" name="tipo">
	HTML;
				while ($row = pg_fetch_row($tipos)) {
					if ($tipo == $row['0']) {
						$str .= "<option value='" . $row['0'] . "' selected>" . $row['1'] . "</option>";
					} else {
						$str .= "<option value='" . $row['0'] . "'>" . $row['1'] . "</option>";
					}
				}
	$str.=<<<HTML
				</select>
			</div>
			<br/>
			<div class="button">
				<button type="submit">Guardar Cambios</button>
			</div>
		</form>
	HTML;

		if (!isset($_POST['nombre']) && !isset($_POST['descripcion'])) {
			echo $str; //Imprimo el formulario cuando no me llega información por Post
		} else {
			$tipo = $_POST['tipo'];
			$marca = $_POST['marca'];
			$nombre = $_POST['nombre'];
			$descripcion = $_POST['descripcion'];
			
			// Se edita el resultado
			$resultado= pg_query($conn, "UPDATE producto SET tipo_id = $tipo, marca_id = $marca, nombre = '$nombre', descripcion = '$descripcion' WHERE producto_id = $id");

			if (!$resultado) {
				echo "Ocurrió un error al consultar";
				exit;
			} else {
				// Se verifica si hubo 1 registro afectado (login correcto). Mostrar registros.
				if (pg_affected_rows($resultado) == 1) {
					header("Location: ejercicio_1_mostrar.php");
				} else {
					echo "ERROR al Editar.<br>";
				}
			}
		}
	}
?>